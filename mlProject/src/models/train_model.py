"""# TF-IDF + LogReg

"""

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import Pipeline
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report, f1_score

from mlProject.src.data.text_classification import final_texts, data

X_train, X_val, y_train, y_val = train_test_split(
    final_texts, data.toxic, test_size=0.3, stratify=data.toxic
)

pipe = Pipeline(
    [
        ("vectorizer", TfidfVectorizer(max_features=5000)),
        ("clf", LogisticRegression()),
    ]
)

pipe.fit(X_train, y_train)

pred = pipe.predict(X_val)
print(classification_report(y_val, pred))