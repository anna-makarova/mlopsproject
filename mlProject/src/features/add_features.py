import click
import pandas as pd
import numpy as np


@click.command()
@click.argument('input_path', type=click.Path(exists=True))
@click.argument('output_path', type=click.Path())
def add_features(input_path: str, output_path: str):
    """Function removes excess columns and enforces
    correct data types.
    :param input_path: Path to read filtered by region DataFrame
    :param output_path: Path to save DataFrame with features
    :return:
    """
    df = pd.read_csv(input_path)
    df["date"] = pd.to_datetime(df["date"])
    # Replace "date" with numeric features for year and month.
    df["year"] = df["date"].dt.year
    df["month"] = df["date"].dt.month
    df.drop("date", axis=1, inplace=True)
    # Apartment floor in relation to total number of floors.
    df["level_to_levels"] = df["level"] / df["levels"]
    # Average size of room in the apartment.
    df["area_to_rooms"] = (df["area"] / df["rooms"]).abs()
    # Fix division by zero.
    df.loc[df["area_to_rooms"] == np.inf, "area_to_rooms"] = df.loc[
        df["area_to_rooms"] == np.inf, "area"
    ]
    df.to_csv(output_path, index=None)


if __name__ == "__main__":
    add_features()
