# -*- coding: utf-8 -*-
"""text_classification.ipynb

Описание данных

# Подготовка данных
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from nltk.tokenize import wordpunct_tokenize
from nltk.corpus import stopwords
from pymorphy2 import MorphAnalyzer
from tqdm import tqdm

data = pd.read_csv("labeled.csv")

data.toxic.value_counts()

texts = list(data.comment)
lengths = [len(wordpunct_tokenize(text)) for text in texts]
lengths_less_than_n = [l for l in lengths if l < 100]
plt.hist(lengths_less_than_n)
plt.show()

np.mean(lengths), np.median(lengths)

words = []
for text in texts:
    words.extend(wordpunct_tokenize(text))
len(words), len(set(words))

len(words), len(set(words))

words = []
for text in texts:
    words.extend(wordpunct_tokenize(text.lower()))
len(words), len(set(words))

len(words), len(set(words))

stop = stopwords.words("russian")
morph = MorphAnalyzer()


def lemmatize(words):
    new_words = []
    for word in words:
        new_words.append(morph.normal_forms(word)[0])
    return new_words


def drop_stopwords(words, stopwords):
    new_words = []
    for word in words:
        if word not in stopwords:
            new_words.append(word)
    return new_words


words = []
for i in tqdm(range(len(texts))):
    words.extend(lemmatize(wordpunct_tokenize(texts[i].lower())))
len(words), len(set(words))

words = []
for i in tqdm(range(len(texts))):
    words.extend(lemmatize(drop_stopwords(wordpunct_tokenize(texts[i].lower()), stop)))
len(words), len(set(words))

preprocessed_texts = []
for i in tqdm(range(len(texts))):
    preprocessed_texts.append(lemmatize(wordpunct_tokenize(texts[i].lower())))
len(words), len(set(words))

final_texts = [" ".join(word for word in text) for text in preprocessed_texts]

len(final_texts)

"""# TF-IDF + LogReg

"""

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import Pipeline
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report, f1_score

X_train, X_val, y_train, y_val = train_test_split(
    final_texts, data.toxic, test_size=0.3, stratify=data.toxic
)

pipe = Pipeline(
    [  # по опыту берем логрег, так как деревяхи хреново работают с эмбеддингами и разреженными векторами
        ("vectorizer", TfidfVectorizer(max_features=5000)),
        ("clf", LogisticRegression()),
    ]
)

pipe.fit(X_train, y_train)

pred = pipe.predict(X_val)
print(classification_report(y_val, pred))
